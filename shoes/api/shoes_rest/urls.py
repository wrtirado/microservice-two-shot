from django.urls import path
from .views import api_list_shoes, api_show_shoes

urlpatterns = [
    path("shoes/", api_list_shoes, name="shoe_list"),
    path("shoes/<int:id>/", api_show_shoes, name="shoe_detail")
]
