# Wardrobify

Team:

* Will Tirado - The Hats Microservice
* Ayram Deliz - Shoes

## Design

## Shoes microservice
Shoe Resource will track the following characteristics:
Example = Characteristic: ModelField to be used.
    manufacturer: CharField
    model name: CharField
    color: CharField
    picture_url: URLField
    wardrobe bin: ForeignKey to BinVO (which will poll....)

## Hats microservice

The hat resource is going to track the hat's fabric, style name, color, a picture, and the location in the wardrobe where it exists. I will create the model for the hats using these required parameters.

* Fabric will be a CharField
* Style will be a CharField
* Color will be a CharField? I'll check if the model fields have another option for this
* Picture will be a URLField
* Location will be a ForeignKey to a LocationVO

There will also need to be a LocationVO in the hats_rest models.py file. I'm going to update that model with with the data from the poller in hats/poll if there is data in the poller that doesn't exist in the LocationVO database inside that microservice. I'll use the same model fields in the LocationVO that I find in the Wardrobe API's model.
