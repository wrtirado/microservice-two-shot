import { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";

function HatsList() {
    const [allHats, setAllHats] = useState([])
    const [refreshKey, setRefreshKey] = useState(0)

    const HatAPI = new FetchWrapper('http://localhost:8090/')

    const fetchData = async () => {
        const hatData = await HatAPI.get('api/hats/')
        setAllHats(hatData.hats)
    }

    useEffect(() => {
        fetchData()
    }, [refreshKey])

    const deleteHat = id => {
        HatAPI.delete(`api/hats/${id}`).then(() => {
            setRefreshKey(oldKey => oldKey + 1)
        }).catch(error => {
            console.error('Error deleting hat:', error)
        })
    };


    return (
    <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Location</th>
                    <th>Section Number</th>
                    <th>Shelf Number</th>
                    <th>Delete?</th>
                </tr>
            </thead>
            <tbody>
                {allHats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.style }</td>
                            <td>{ hat.location }</td>
                            <td>{ hat.section_number }</td>
                            <td>{ hat.shelf_number }</td>
                            <td onClick={() => deleteHat(hat.id)}><button className="btn btn-danger">Delete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default HatsList
