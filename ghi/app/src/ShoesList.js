import { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";

function ShoesList() {
    const [allShoes, setAllShoes] = useState([])
    const [refreshKey, setRefreshKey] = useState(0)

    const ShoesAPI = new FetchWrapper("http://localhost:8080/")

    const fetchData = async () => {
        const shoesData = await ShoesAPI.get("api/shoes/")
        setAllShoes(shoesData.shoes)
    }

    useEffect(() =>{
        fetchData()
    },[refreshKey])

    const deleteShoes = id => {
        ShoesAPI.delete(`api/shoes/${id}`)
        .then(() => {setRefreshKey(oldKey => oldKey +1)
        }).catch(error => {
            console.error("Error deleting shoes:", error)
        })
    };

    return (
    <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Bin</th>
                    <th>Delete?</th>
                </tr>
            </thead>
            <tbody>
                {allShoes.map(shoes => {
                    return (
                        <tr key={shoes.id}>
                            <td>{shoes.manufacturer}</td>
                            <td>{shoes.model}</td>
                            <td>{shoes.color}</td>
                            <td>{shoes.bin}</td>
                            <td onClick={() => deleteShoes(shoes.id)}><button className="btn btn-danger">Delete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default ShoesList
