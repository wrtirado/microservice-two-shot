import React, { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";


function ShoesForm(){
    const [bins, setBins] = useState([])
    const [manufacturer, setManufacturer] = useState("")
    const [model_name, setModel_Name] = useState("")
    const [color, setColor] = useState("")
    const [picture_url, setPicture_Url] = useState("")
    const [bin, setBin] = useState("")

    const WardrobeAPI = new FetchWrapper("http://localhost:8100/")
    const ShoesAPI = new FetchWrapper("http://localhost:8080/")

    const fetchData = async () => {
        const binData = await WardrobeAPI.get("api/bins/")
        setBins(binData.bins)
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.manufacturer = manufacturer
        data.model_name = model_name
        data.color = color
        data.picture_url = picture_url
        data.bin = bin

        const newShoes = await ShoesAPI.post("api/shoes/", data)
        setManufacturer("")
        setModel_Name("")
        setColor("")
        setPicture_Url("")
        setBin("")
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModel_Name(value)
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPicture_Url(value)
    }

    const handleBinChange = (event) => {
        const value = event.target.value
        setBin(value)
    }

    return(
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create New Shoes</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" value={manufacturer} name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleModelNameChange} placeholder="Model Name" required type="text" value={model_name} name="model name" id="model name" className="form-control" />
                            <label htmlFor="model name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" value={color} name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} placeholder="Picture Url" required type="text" value={picture_url} name="Picture Url" id="Picture Url" className="form-control" />
                            <label htmlFor="picture url">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleBinChange} required value={bin} name="bin" id="bin" className="form-select">
                                <option value="">Choose a Bin</option>
                                {bins.map(bin => {
                                    return(
                                        <option key={bin.id} value={bin.href}>
                                            {bin.bin_number}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default ShoesForm
