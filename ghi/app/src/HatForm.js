import React, { useEffect, useState } from 'react'
import { FetchWrapper } from './fetch-wrapper'

function HatForm() {
    const [locations, setLocations] = useState([])
    const [fabric, setFabric] = useState('')
    const [style, setStyle] = useState('')
    const [color, setColor] = useState('')
    const [image, setImage] = useState('')
    const [location, setLocation] = useState('')

    const WardrobeAPI = new FetchWrapper('http://localhost:8100/')
    const HatAPI = new FetchWrapper('http://localhost:8090/')

    const fetchData = async () => {
        const locationsData = await WardrobeAPI.get('api/locations/')
        setLocations(locationsData.locations)
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.fabric = fabric
        data.style = style
        data.color = color
        data.picture = image
        data.location = location

        const newHat = await HatAPI.post('api/hats/', data)
        setFabric('')
        setStyle('')
        setColor('')
        setImage('')
        setLocation('')
    }

    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value)
    }

    const handleStyleChange = (event) => {
        const value = event.target.value
        setStyle(value)
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleImageChange = (event) => {
        const value = event.target.value
        setImage(value)
    }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }


    return (
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} placeholder="Fabric" required type="text" value={fabric} name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleChange} placeholder="Style" required type="text" value={style} name="style" id="style" className="form-control" />
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" value={color} name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleImageChange} placeholder="Image URL" required type="url" value={image} name="image" id="image" className="form-control" />
                            <label htmlFor="image">Image</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} required value={location} name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.href}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default HatForm
